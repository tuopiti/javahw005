public class ThreadDemo {
    public static void main(String[] args) {
        System.out.println("Java greeting:");
        try{
            String name = "Hello KSHRD!";
            for(int i = 0; i < Math.min(name.length(), name.length()); i++){
                System.out.print(name.charAt(i));
                Thread.sleep(300);
            }
            System.out.println();
            for (int n=37;n>0;n--){
                System.out.print("*");
                Thread.sleep(250);
            }
            System.out.println();
            String letters = "I will try my best to be here at HRD.";
            for(int i = 0; i < Math.min(letters.length(), letters.length()); i++){
                System.out.print(letters.charAt(i));
                Thread.sleep(300);
            }
            System.out.println();
            for (int n=37;n>0;n--){
                System.out.print("-");
                Thread.sleep(250);
            }
            System.out.println();
            System.out.print("Downloading");
            for (int n=9;n>0;n--){
                System.out.print(".");
                Thread.sleep(300);
            }
            System.out.print("Completed 100%!");
        }catch (InterruptedException e){
            System.out.println("Main thread interrupted");
        }

    }
}
